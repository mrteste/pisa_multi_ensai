---
title: "Score en lecture et statut de l'établissement"
output:
  html_document:
    toc: true
    toc_depth: 2
    toc_float:
      collapsed: false
      smooth_scroll: false
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, fig.height = 7, fig.width = 9)
```

# Introduction  

Les lycéens des filières générale et technologique du privé ou du public ont des scores relativement proches en moyenne (résultat étude semestre 1). Mais au sein du public et du privé, les établissements peuvent être très divers. Les moyennes globales par statut d'établissement ne permettent pas de rendre compte de l'hétérogénéïté qui existe dans le public ou dans le privé.   

Nous nous proposons ainsi de renouveler l'étude du lien entre le statut de l'établissement et le score des élèves en utilisant des méthodes exploratoires multivariées qui permmettent de prendre en compte un grand nombre de variables, afin de déterminer si parmi les lycéens se distinguent des profils associant score en lecture et statut de l'établissement.



# Préliminaires  


```{r echo = FALSE, include=FALSE}
## Chargement des données et des packages (pour nous)
suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(ggthemes))
suppressPackageStartupMessages(library(FactoMineR))
suppressPackageStartupMessages(library(reldist))
suppressPackageStartupMessages(library(cluster))
suppressPackageStartupMessages(library(FactoClass))

racine_repertoire <- "G:"
chemin_data <- paste(racine_repertoire, "pisa_multi_ensai", "data/", sep = "/")
p <- sapply(list.files(path = chemin_data, pattern = ".RData", full.names = TRUE), load, envir = .GlobalEnv)
#récupération des numéros des individus uniques de la table pondérées
ind <- lyceensGT_pond %>% pull(student_ident) %>% unique()
indr <- sapply(ind, function(x) which(lyceensGT_pond$student_ident == x)[1])

#lyceensGT : table "normale"
#lyceensGT_pond : table où les lignes sont répétées autant que le poids
#acm_nopond : résultat des ACM sur la table lyceensGT contient l'ACM avec na et avec average
#acm_pond : résultat des ACM sur la table lyceensGT_pond contient l'ACM avec na et avec average
# str(acm_nopond,max.level = 2) #pour utiliser l'acm_nopond avec la méthode average : acm_nopond$av

source_pond_sansna <- "ACM sur données pondéres et traitement automatique de la non réponse"
source_nopond_sansna <- "ACM sur données non pondérées avec retrait des NA"

indna <- apply(is.na(lyceensGT), MARGIN = 1, FUN= sum)
lyceensGT_sansNA <- lyceensGT[indna == 0,]
nb_indna <- table(indna>0)
pc_indna <- prop.table(table(indna>0))*100
data_etud <- lyceensGT_sansNA %>% select_if(is.factor) %>%
  select(-stulevel,-sturep, -studesk, -stuquiet, -studict)
contin_schpbens_stuscor_pond <- contin_desk_statut_pond
contin_schpinfra_stuscor_pond <- contin_dict_statut_pond

```


## Présentation des données et quelques statistiques descriptives

###Type des données

```{r}
str(lyceensGT)
```


###Résumé des données

```{r}
summary(lyceensGT_pond %>% select_if(is.factor))
```

```{r}
knitr::kable(contin_score_statut_pond,
             caption = "Niveau en lecture (lignes) et Statut de l'établissement (colonnes)")
```


```{r}
knitr::kable(contin_score_famnivsoc_pond,caption = "Niveau en lecture (lignes) et niveau social (colonnes) (en %)")
```

```{r}
knitr::kable(contin_score_aimed_pond,caption = "Niveau en lecture et niveau de diplôme visé (en %)")
```

```{r, fig.height=5}

ggplot(lyceensGT %>% filter(!is.na(stuaimed))) +
  geom_bar(aes(x=stuscor,fill=stuaimed,weight=student_weight), width = 0.75, stat = "count", position = "fill") +
  coord_flip() +
  labs(x="part",y="niveau de score")+
  scale_fill_brewer("Diplôme visé par le lycéen", type="qual", palette = 3) +
    guides(fill = guide_legend(title.position = "top")) +
  ggtitle("Diplôme visé par les lycéens du GT selon leur niveau en lecture") +
  ggthemes::theme_economist_white(base_size = 8)
```


```{r}
knitr::kable(contin_score_schratio_pond,caption = "Niveau en lecture et ratio étudiant/prof dans l'établissement (en %)")
```


```{r}
knitr::kable(contin_schpbens_stuscor_pond,caption = "Manque d'enseignants dans l'établissement et Niveau en lecture (en %)")
```

```{r}
knitr::kable(contin_schpinfra_stuscor_pond,caption = "Problème d'infrastructure dans l'établissement et Niveau en lecture (en %)")
```

```{r, fig.height=4}

ggplot(lyceensGT) +
  geom_bar(aes(x=genre,fill=stuscor,weight=student_weight), width = 0.75, stat = "count", position = "fill") + coord_flip() +
  scale_fill_brewer("Niveau en lecture",type="qual", palette=3) +
  labs(x="genre",y="part") +
  guides(fill = guide_legend(title.position = "top")) +
  ggthemes::theme_economist_white()
```


```{r}
# image(as.matrix(cor_mat))
ggplot(as.data.frame(V.cramer) %>% mutate(var1=colnames(V.cramer)) %>% gather(var2, cor, -var1)) +
  geom_raster(aes(x=var1,y=var2,fill=cor), alpha = 0.9) +
  scale_fill_gradient2("V de Cramer") +
  labs(x="",y="") +
  ggtitle("V de Cramer entre quelques variables", 
          subtitle = "Plus la couleur est foncée, plus le V de Cramer est proche de 1,\nplus les variables croisées sont liées entre elles") +
  theme_bw()
```



# Visualisations par méthode factorielle  

Les données étudiantes sont pondérées et comportent des données manquantes.  

Les données manquantes peuvent être éliminées soit par imputation soit en supprimant tout individu ayant une donnée manquante. L'imputation nécessite de disposer d'une méthode robuste d'imputation, la seconde peut déformer le nuage des individus et faire perdre beaucoup d'informations. Dans notre cas, il y a `r nrow(lyceensGT)` lycéens sondés au total et  reste `r nb_indna["FALSE"]` lycéens sondés ayant répondu à l'ensemble des questions, soit `r round(pc_indna["FALSE"],2)`% des lycéens. Nous testerons l'ACM avec et sans les valeurs manquantes pour étudier si leur influence se fait sentir sur les résultats.
L'imputation nécessite une méthode robuste. Sans chercher à en mettre une en place, nous avons à notre disposition deux méthodes d'imputation développées par les créateurs du package FactoMineR. En effet, la fonction `MCA()` dispose d'un argument `na.method` qui prend deux valeurs possibles :  
- `na.method = "NA"` : une modalité supplémentaire sera créée pour chaque variable disposant de valeurs manquantes.  
- `na.methode = "Average"` : la fonction `MCA` se charge ainsi d'imputer automatiquement les valeurs manquantes par une modalité moyenne.  


La fonction `FactoMineR::MCA()` dispose d'un argument permettant de pondérer les individus. Cette pondération n'étant pas possible lors des méthodes de clustering, nous chercherons, lors de la mise en place des méthodes factorielles, si des différences importantes apparaissent entre les résultats sur données pondérées et sur données non pondérées. Si, peu de différences sont notables, nous privilégieons l'étude des données non pondérées car plus pertinentes pour le clustering.  

Pour faire le choix entre ces quatre, nous nous proposons de comparer les différentes acm sur les deux premiers plans factoriels de l'analyse.

### Les pondérations modifient très peu les positions relatives des modalités sur les deux premiers plans

```{r}
par(mfrow = c(2,2))
plot(acm_nopond$na, invisible = "ind", cex = 0.8, title = "Données non pondérées\nprises en compte des NA")
plot(acm_pond$na, invisible = "ind", cex = 0.8,title="Données pondérées\nprises en compte des NA")
plot(acm_nopond$av, invisible = "ind", cex = 0.8,title="Données non pondérées\nimputation des NA")
plot(acm_pond$av, invisible = "ind", cex = 0.8,title="Données pondérées\nimputation des NA")
```

Sur ce premier plan factoriel, on remarque que les nuages pondérés ou non pondérés sont très proches, avec prise en compte des valeurs manquantes ou non.

```{r, fig.height=10}
par(mfrow = c(2,2))
plot(acm_nopond$na, invisible = "ind", cex = 0.8, axes = 3:4, title = "Données non pondérées\nprises en compte des NA")
plot(acm_pond$na, invisible = "ind", cex = 0.8, axes = 3:4,title="Données pondérées\nprises en compte des NA")
plot(acm_nopond$av, invisible = "ind", cex = 0.8, axes = 3:4,title="Données non pondérées\nimputation des NA")
plot(acm_pond$av, invisible = "ind", cex = 0.8, axes = 3:4,title="Données pondérées\nimputation des NA")
```

Sur le second plan factoriel, les nuages pondérés ou non ne sont pas distinguables.

=> On travaillera par la suite sur les données non pondérées, car elles seront directement utilisables pour le clustering

### Les valeurs manquantes

```{r}
par(mfrow = c(2,2))
plot(acm_nopond$na, invisible = "ind", cex = 0.5, title = "Données non pondérées\nprises en compte des NA")
plot(acm_nopond$na, invisible = "ind", cex = 0.5, axes = 2:3,title="Données non pondérées\nprises en compte des NA")
plot(acm_nopond$av, invisible = "ind", cex = 0.5, title = "Données non pondérées\nimputation des NA")
plot(acm_nopond_sansna$na, invisible = "ind", cex = 0.5,title="Données non pondérées\nretrait préalable des NA")
```

On remarque que les plans factoriels des 3 derniers graphiques sont très proches. Ainsi en comparant le plan 2-3 de l'analyse avec NA aux plans 1-2 des analyses avec, respectivement, imputation automatique (graphique 3) ou retrait des NA (graphique 4), nous observons les mêmes positions relatives des modalités, ce qui signale une robustesse de l'analyse quelque soit le choix fait.  
=> Nous pouvons ainsi faire le choix de retirer les valeurs manquantes de l'analyse, d'autant qu'on ne retire par là que 8% des individus sondés.  

Mais le premier graphique montre une atypicité importante des valeurs manquantes sur certaines variables. Nous nous proposons d'analyser rapidement ce phénomène et les individus qui peuvent être concernés.

```{r}
plot(acm_nopond$na, cex = 0.8, label = "var", title = "Données non pondérées + prises en compte des NA")
```

Le graphique montre des individus très séparés du reste du nuage de point.

```{r}
lyceens_aberr <- lyceensGT[acm_nopond$na$ind$coord %>% as.data.frame() %>% mutate(aberr = `Dim 1` > 2) %>% pull(aberr),]
summary(lyceens_aberr[-c(1:3,12,14)])
# summary(lyceensGT[-c(1:3,12,14)])
# table(lyceens_aberr$school_ident) plusieurs établissements concernés
```

Les 26 individus les plus atypiques sur l'axe 1 de l'analyse factorielle n'ont répondu à aucune question sur leur environnement familial. On ne dispose pas non plus de leur niveau social. Sur les autres variables, nous observons que ces individus proviennent de lycées publics de villes moyennes ou grandes dont les proviseurs déclarent avoir des problèmes de matériel, d'infrastructure, d'équipe, qui ont un ratio étudiant/prof plus bas que la moyenne pour plus de la moitié d'entre eux. De plus, les élèves concernés ont obtenu des scores plus faibles que la moyenne sauf 5 d'entre eux.

```{r}
# lyceens_part <- lyceensGT[acm_nopond$na$ind$coord %>% as.data.frame() %>% mutate(aberr = `Dim 1` > 0.5 & `Dim 1`< 2) %>% pull(aberr),]
# summary(lyceens_part[-c(1:3,12,14)])
#table(lyceens_part$school_ident) #plusieurs établissements
```


###Bilan du choix des données pour l'analyse

Nous concentrons notre étude sur les lycéens du GT dont les données sur les variables ne sont pas manquantes. A part quelques individus très atypiques ayant répondu à aucune question sur leur environnement familial, nous pouvons restreindre l'analyse, sans déformer outre mesure le nuage et l'analyse factorielle, aux individus non pondérés pour lesquels l'ensemble des variables est renseigné.  

Nous conservons une table de données composées de `r dim(lyceensGT_sansNA)[1]` individus et de `r dim(lyceensGT_sansNA %>% select_if(is.factor))[2]` variables qualitatives.

```{r}
p <- dim(lyceensGT_sansNA)
```



## Choix des éléments actifs/supplémentaires  

Nous ne plaçons aucun individu en supplémentaire dans un premier temps.  

La présentation des variables statistiques de notre table montre que certaines variables ont des modalités très rares. C'est le cas notamment des variables liées à l'environnement familial : 

```{r}
variables <- c("studesk","stuquiet","sturep","studict","sturoom")
tables <- lapply(variables, FUN = function(var) prop.table(table(lyceensGT_sansNA[[var]]))*100)

knitr::kable(tables[[1]],digits = 1,col.names = c("studesk", "%"))
knitr::kable(tables[[2]],digits = 1,col.names = c("stuquiet", "%"))
knitr::kable(tables[[3]],digits = 1,col.names = c("sturep", "%"))
knitr::kable(tables[[4]],digits = 1,col.names = c("studict", "%"))
knitr::kable(tables[[5]],digits = 1,col.names = c("sturoom", "%"))
```

Ces variables, du fait de leur rareté, se démarquent fortement sur le premier plan factoriel et contribuent très fortement à la construction des 3èmes et 5èmes axes.

```{r}
contrib15 <- acm_nopond_sansna$av$var$contrib[,1:5] %>% as.data.frame() %>% mutate_all(funs(round), digits = 3)
row.names(contrib15) <- row.names(acm_nopond_sansna$av$var$contrib)
names(contrib15) <- paste0("dim",1:5)

DT::datatable(contrib15 %>% slice(which(row.names(contrib15) %in% paste(variables, c(rep("n",2),"o",rep("n",2)), sep="_"))),rownames = TRUE, caption = "Contribution (en %) des modalités rares à la construction des 5 premiers axes")
```

Nous choisissons de placer en variable supplémentaire les variables très rares telles que : studesk, studict, stuquiet et sturep.

### Récapitulatif des choix opérés pour réaliser l'analyse factorielle

Le nuage de données est composée de `r dim(lyceensGT_sansNA)[1]` individus, tous actifs, de `r dim(lyceensGT_sansNA %>% select_if(is.factor))[2]-4` variables qualitatives actives et de 4 variables qualitatives placées en supplémentaire.  
Les données analysées seront pondérées par des poids identiques.
Les données de l'analyse étant qualitatives, nous procédons à une ACM avec la métrique du $\chi^2$.  
L'ACM sera réalisée à partir du tableau disjonctif complet et non sur le tableau de Burt afin de conserver lapossibilité d'étudier les individus. Mais, dans ce cas, l'inertie n'est pas interprétable comme mesure de l'intensité des liens entre modalités.


### Choix du nombre d'axes

```{r}
barplot(acm_nopond_sansna_qualisup$av$eig[,1])
title("Graphique des valeurs propres de l'ACM")

```
D'après le critère du coude, nous choisissons de nous concentrer sur les cinq premiers axes de l'ACM.

## Premier plan de l'ACM

### Graphique des individus
```{r}
ind_coord <- as.data.frame(acm_nopond_sansna_qualisup$av$ind$coord[,1:5])
names(ind_coord) <- paste0("dim",1:5)
individus <- lyceensGT_sansNA %>% bind_cols(ind_coord)

ggplot(individus, aes(x=dim1,y=dim2)) +
  geom_point(aes(shape=schstat, col=stuscor), alpha=0.5, size=2) +
  scale_color_brewer(type="seq",palette = 3) +
  facet_wrap(~schstat,nrow = 2) +
  geom_hline(yintercept = 0) +
  geom_vline(xintercept = 0) +
  ggtitle("Sur le premier axe factoriel, un gradient de score semble apparaître.
          Une différence public/privé est également visible."
          ,"Graphique des individus uniques") +
  ggthemes::theme_calc()
```

Selon ce premier plan, un gradient de score semble se dessiner sur le premier axe, ainsi qu'une séparation entre privé et public. Nous allons confirmer cette impression par l'analyse des résultats de l'ACM sur ce premier plan, et voir si d'autres modalités sont fortement liées.

### Les variables et modalités les plus liées au premier plan

```{r}
plot(acm_nopond_sansna_qualisup$av, choix = "var", invisible = "ind", cex = 0.8)
title("Corrélation des variables avec les axes du premier plan factoriel")
plot(acm_nopond_sansna_qualisup$av, invisible = "ind", cex = 0.8,title = "Projection des modalités sur le premier plan factoriel")
```

Le graphique des modalités sur le premier plan montre  que :  

- le premier axe oppose les modalités stulitt_n sturoom_n infbac famnivsoc_niv1 stuscor_niv1 schdisav_q4 aux modalités pr (privé) schdisav_q1 famnivsoc_niv4 stuscor_niv4.  
En résumé, sur la droite, les individus les plus défavorisés socialement et ayant les moins bons scores en lecture, ne disposant pas de chambre à part, ni de littérature à la maison, inscrits dans des lycées où il y a les plus fortes proportions d'élèves socialement désavantagés. Il s'agit également d'élèves ne se projetant même pas avoir le baccalauréat, alors qu'ils sont en seconde. Ils sont donc trs pessimistes dans leur chance de succès au bac. => Sur la droite les élèves cumulant les difficultés sociales et scolaires.  
Sur la gauche de l'axe, on retrouve les élèves du privé au fort capital social, obtenant les meilleurs scores en lecture et inscrit dans des lycées ayant les moins fortes proportions d'élèves défavorisés. Ce sont aussi des élèves se projetant vers des diplômes de l'enseignement supérieur long (au-delà de la licence).
On retrouve un clivage entre les deux formes extrèmes d' "entre soi".

- le second axe oppose en haut les modalités liées aux établissements de petites villes (moins de 15000 habitants) rencontrant des problèmes en tout genre (matériel, infrastructure, enseignant, staff) qui ont parmi les plus fortes proportions d'élèves dans le besoin (schneeds_q3) aux modalités, en bas, associées aux établissements des très grandes ou grandes agglomérations n'ayant pas particulièrement de problèmes.  
En résumé, le second axe sépare les établissements des grands agglo ne rencontrant pas de problèmes particuliers aux établissements des petites villes cumulant les problèmes.  

Sur ce plan, les établissements publics et privés se séparent assez bien sur le premier axe mais moins sur le second.  

Vérifions avec les contributions et les cos2 (qualité de la représentation), si ces analyses sont pertinentes.

####Les contributions des modalités 

Il y a 45 modalités = > la contribution moyenne est de `r round(1/53*100,1)`%.
```{r}
contrib15 <- acm_nopond_sansna_qualisup$av$var$contrib[,1:5] %>% as.data.frame() %>% mutate_all(funs(round), digits = 3)
row.names(contrib15) <- row.names(acm_nopond_sansna_qualisup$av$var$contrib)
names(contrib15) <- paste0("dim",1:5)

DT::datatable(contrib15,rownames = TRUE, caption = "Contribution des modalités à la construction des 5 axes retenus")
```

```{r}
ggplot(contrib15 %>%
         mutate(mod = row.names(contrib15))
         , aes(x = reorder(mod, dim1, sum), y = dim1)) +
  geom_bar(stat = "identity") +
  coord_flip() +
  geom_hline(yintercept = 1/53*100, col = "orangered") +
  labs(y = "% de contribution", x = "modalités", caption = source_pond_sansna) +
  ggtitle("Contributions des modalités à la construction de l'axe 1") +
  ggthemes::theme_hc()
```

Les modalités pr, schdisav_q1 et q4, stulitt_n, stuscor_niv1 et niv4, famnivsoc_niv1 et 4, infbac et lic+ contribuent fortement à l'axe 1

```{r}
ggplot(contrib15 %>%
         mutate(mod = row.names(contrib15))
         , aes(x = reorder(mod, dim2, sum), y = dim2)) +
  geom_bar(stat = "identity") +
  coord_flip() +
  geom_hline(yintercept = 1/53*100, col = "orangered") +
  labs(y = "% de contribution", x = "modalités", caption = source_pond_sansna) +
  ggtitle("Contributions des modalités à la construction de l'axe 2") +
  ggthemes::theme_hc()
```

Les modalités liées aux problèmes rencontrés par les établissements ainsi que celles liées à la taille de la ville d'implantation du lycée contribuent fortement à l'axe 2. Les autres variables ne contribuent presque pas à la construction de cet axe.

####La qualité de représentation des modalités
```{r}
DT::datatable(acm_nopond_sansna_qualisup$av$var$cos2[,1:5] %>%
                as.data.frame() %>%
                mutate(total_15 = `Dim 1`+`Dim 2`+`Dim 3`+`Dim 4`+`Dim 5`) %>%
                mutate_all(funs(round), digits = 3),
              rownames = row.names(acm_nopond_sansna_qualisup$av$var$cos2),
              caption = "Qualité de représentation des modalités par les 5 premiers axes de l'ACM")
```

Le tableau des cos2 montre que les variables contributrices pour les axes 1 ou 2 sont aussi les mieux représentées par ces axes. Pour l'axe 2, ce sont surtout les modalités schpbstaff et schpbmat qui sont les mieux représentées.

Ces deux derniers tableaux, contribution et cos2, confirment donc que l'analyse réalisée précédemment est pertinente.


```{r}
mod_coord <- as.data.frame(acm_nopond_sansna_qualisup$av$var$coord[,1:5]) %>%
  mutate(mod = row.names(acm_nopond_sansna_qualisup$av$var$coord[,1:5]))
names(mod_coord) <- c(paste0("coord_",1:5),"mod")

mod_cos2 <- as.data.frame(acm_nopond_sansna_qualisup$av$var$cos2[,1:5]) %>%
  mutate(mod = row.names(acm_nopond_sansna_qualisup$av$var$cos2[,1:5]))
names(mod_cos2) <- c(paste0("cos2_",1:5),"mod")

mod_cos2 %<>%  left_join(mod_coord, by = "mod") %>% mutate(repr12 = (cos2_1 + cos2_2) > 0.2)
ggplot(mod_cos2, aes(x=coord_1,y=coord_2)) +
  geom_point(aes(col = repr12, alpha=ifelse(repr12,0.75,0.25)), size=2) +
  geom_text(data = mod_cos2 %>% filter(coord_1 < 0),
            aes(label = mod, alpha=ifelse(repr12,1,0)),
            size = 4, hjust = -0.1) +
  geom_text(data = mod_cos2 %>% filter(coord_1 >= 0),
            aes(label = mod, alpha=ifelse(repr12,1,0)),
            size = 4, hjust = 1) +
  ggthemes::scale_color_fivethirtyeight("Cos2 > 0.2") +
  ggtitle("Premier plan factoriel","Graphique des modalités") +
  geom_vline(xintercept = 0) +
  geom_hline(yintercept = 0) +
  guides(color = guide_legend(order=1, title.position = "top"),
         # size = guide_legend(order=2),
         alpha = FALSE) +
  labs(caption=source_nopond_sansna) +
  ggthemes::theme_economist_white() +
  theme(panel.grid = element_blank(), axis.line = element_blank(), axis.ticks.y = element_line(size=0.5))
```


## Second plan de l'ACM 

### Graphique des individus
```{r}
ggplot(individus, aes(x=dim3,y=dim4)) +
  geom_point(aes(shape=schstat, col=stuscor), alpha=0.5, size=2) +
  scale_color_brewer(type="seq",palette = 3) +
  facet_wrap(~schstat,nrow = 2) +
  geom_hline(yintercept = 0) +
  geom_vline(xintercept = 0) +
  ggtitle("Second plan factoriel"
          ,"Graphique des individus uniques") +
  ggthemes::theme_calc()
```

Le score ou le statut de l'établissement ne semblent pas discriminer les individus sur ce plan.

###Les modalités

```{r}
ggplot(contrib15 %>%
         mutate(mod = row.names(contrib15))
         , aes(x = reorder(mod, dim3, sum), y = dim3)) +
  geom_bar(stat = "identity") +
  coord_flip() +
  geom_hline(yintercept = 1/53*100, col = "orangered") +
  labs(y = "% de contribution", x = "modalités", caption = source_pond_sansna) +
  ggtitle("Contributions des modalités à la construction de l'axe 3") +
  ggthemes::theme_hc()
```


Le 3eme axe factoriel est construit majoritairement par les modalités de la variable de taille de la ville où est implanté le lycée, de la variable ratio étudiant/prof, les modalités q1 et q3 du taux d'élèves défavorisés par établissement. La modalité non de la variable sturoom contribue également largement plus que la moyenne.


```{r}
ggplot(contrib15 %>%
         mutate(mod = row.names(contrib15))
         , aes(x = reorder(mod, dim4, sum), y = dim4)) +
  geom_bar(stat = "identity") +
  coord_flip() +
  geom_hline(yintercept = 1/53*100, col = "orangered") +
  labs(y = "% de contribution", x = "modalités", caption = source_pond_sansna) +
  ggtitle("Contributions des modalités à la construction de l'axe 4") +
  ggthemes::theme_hc()
```


Sur le quatrième axe, on distingue les contributions de modalités de variables d'établissement tels que schneeds_q4, famnivsoc_niv4, ratio étudiant/prof et la variable schpbens

```{r}
mod_cos2 %<>% mutate(repr34 = (cos2_3 + cos2_4) > 0.2)
ggplot(mod_cos2, aes(x=coord_3,y=coord_4)) +
  geom_point(aes(col = repr34, alpha=ifelse(repr34,0.75,0.25)), size=2) +
  geom_text(data = mod_cos2 %>% filter(coord_3 < 0),
            aes(label = mod, alpha=ifelse(repr34,1,0)),
            size = 4, hjust = -0.1) +
  geom_text(data = mod_cos2 %>% filter(coord_3 >= 0),
            aes(label = mod, alpha=ifelse(repr34,1,0)),
            size = 4, hjust = 1) +
  ggthemes::scale_color_fivethirtyeight("Cos2 > 0.2") +
  ggtitle("Deuxième plan factoriel","Graphique des modalités") +
  geom_vline(xintercept = 0) +
  geom_hline(yintercept = 0) +
  guides(color = guide_legend(order=1, title.position = "top"),
         # size = guide_legend(order=2),
         alpha = FALSE) +
  labs(caption=source_pond_sansna) +
  ggthemes::theme_economist_white() +
  theme(panel.grid = element_blank(), axis.line = element_blank(), axis.ticks.y = element_line(size=0.5))
```

```{r}
plotellipses(acm_nopond_sansna_qualisup$na, keepvar = c(1,9,10,11,12,14,18,20), label = "none")
plotellipses(acm_nopond_sansna_qualisup$na, keepvar = c(1,9,10,11,12,14,18,20), label = "none", axes = 3:4)
```


## Le 5eme axe

```{r}
ggplot(contrib15 %>%
         mutate(mod = row.names(contrib15))
         , aes(x = reorder(mod, dim5, sum), y = dim5)) +
  geom_bar(stat = "identity") +
  coord_flip() +
  geom_hline(yintercept = 1/53*100, col = "orangered") +
  labs(y = "% de contribution", x = "modalités", caption = source_pond_sansna) +
  ggtitle("Contributions des modalités à la construction de l'axe 5") +
  ggthemes::theme_hc()
```



```{r}
mod_cos2 %<>% mutate(repr15 = (cos2_1 + cos2_5) > 0.2)
ggplot(mod_cos2, aes(x=coord_1,y=coord_5)) +
  geom_point(aes(col = repr15, alpha=ifelse(repr15,0.75,0.25)), size=2) +
  geom_text(data = mod_cos2 %>% filter(coord_1 < 0),
            aes(label = mod, alpha=ifelse(repr15,1,0)),
            size = 4, hjust = -0.1) +
  geom_text(data = mod_cos2 %>% filter(coord_1 >= 0),
            aes(label = mod, alpha=ifelse(repr15,1,0)),
            size = 4, hjust = 1) +
  ggthemes::scale_color_fivethirtyeight("Cos2 > 0.2") +
  ggtitle("Plan factoriel composés des axes 1 et 5","Graphique des modalités") +
  geom_vline(xintercept = 0) +
  geom_hline(yintercept = 0) +
  guides(color = guide_legend(order=1, title.position = "top"),
         # size = guide_legend(order=2),
         alpha = FALSE) +
  labs(caption=source_pond_sansna) +
  ggthemes::theme_economist_white() +
  theme(panel.grid = element_blank(), axis.line = element_blank(), axis.ticks.y = element_line(size=0.5))
```

## Clustering

Pour opérer une classification sur nos données qualitatives, nous nous proposons de comparer les résultats d'une CAH et d'un modèle de mélange multinomial.

### CAH

La classification ascendante hiérarchique sur données qualitatives nécessite d'utiliser une distance non euclidienne adaptée aux variables qualitatives. Nous utiliserons la distance du $\chi2$ et la distance de Gower. 

```{r, echo = TRUE, include=TRUE}
pltree(res_cah$gower,main = "Dendrogramme issu d'une cah réalisée avec la distance de Gower et le critère de Ward",labels = FALSE)
pltree(res_cah$chi2, main = "Dendrogramme issu d'une cah réalisée avec la distance du chi2 et le critère de Ward", labels = FALSE)
pltree(res_cah$manhattan, main = "Dendrogramme issu d'une cah réalisée avec la distance de manhattan et le critère de Ward", labels = FALSE)

resume_cah <- function(part, data){
  data <- data %>% mutate(partition = factor(part, ordered = TRUE))
  by(data, data$partition, summary)
}
# resume_cah_2c <- lapply(names(part_cah_2c),FUN = function(x) resume_cah(part = part_cah_2c[[x]], data = data_etud))
# names(resume_cah_2c) <- names(part_cah_2c)


plot(sort(res_cah$chi2$height, decreasing = TRUE)[1:10], type = "s",
     main = "Sauts d'inertie intra-classe selon le nombre de classes retenues\nCAH réalisée à partir de la distance du chi2",
     xlab = "Nombre de classes", ylab = "Inertie intra-classe")
plot(sort(res_cah$gower$height, decreasing = TRUE)[1:10], type = "s",
     main = "Sauts d'inertie intra-classe selon le nombre de classes retenues\nCAH réalisée à partir de la distance de Gower",
     xlab = "Nombre de classes", ylab = "Inertie intra-classe")
plot(sort(res_cah$manhattan$height, decreasing = TRUE)[1:10], type = "s",
     main = "Sauts d'inertie intra-classe selon le nombre de classes retenues\nCAH réalisée à partir de la distance de Manhattan",
     xlab = "Nombre de classes", ylab = "Inertie intra-classe")

#les diagrammes d'inertie montrent que pour chi2, le nombre de classes le plus pertinent serait 5, et 4 pour les deux autres.
#Neanmoins l'arbre du chi2 montre que le passage de 4 à 5 classes sépare une classe ayant peu d'individus en 2 sous classes. 
#Ainsi, cette classe regroupe des individus assez différents mais semble trop petites pour être sous divisées

part_cah <- function(cah, nbclasse) cutree(cah, nbclasse)
part_cah_4c <- lapply(res_cah, part_cah, nbclasse = 4)

resume_cah_4c <- lapply(names(part_cah_4c),FUN = function(x) resume_cah(part = part_cah_4c[[x]], data = data_etud))
names(resume_cah_4c) <- names(part_cah_4c)

resume_cah_4c




```


Comparaison des trois partitions par CAH
```{r}
library(VarSelLCM)

table(part_cah_4c$chi2,part_cah_4c$gower, deparse.level = 2)
table(part_cah_4c$chi2,part_cah_4c$manhattan, deparse.level = 2)
table(part_cah_4c$manhattan,part_cah_4c$gower, deparse.level = 2)

ARI(part_cah_4c$chi2,part_cah_4c$gower)
ARI(part_cah_4c$chi2,part_cah_4c$manhattan)
ARI(part_cah_4c$manhattan,part_cah_4c$gower)
```


##### Modèle de mélange

```{r}

#les pik 
coef(res_melange)@pi

#les alphak par modalités
coef(res_melange)@paramCategorical@alpha

#les variables les plus discriminantes
plot(res_melange, type = "bar") + coord_flip()

#les risques de mauvais classement semblent assez réduits
plot(res_melange, type = "probs-class")
lapply(1:4, function(x) table(between(fitted(res_melange, type="probability")[,x], 0.5, 1)))
lapply(1:4, function(x) table(between(fitted(res_melange, type="probability")[,x], 0.25, 0.5)))

#récupération de la partition (MAP)
part_4c_mel <- fitted(res_melange, type="partition")
#part_4c_mel <- res_melange@partitions@zMAP

#Comparaison avec les différentes partitions issues d'une CAH

ARI(part_4c_mel, part_cah_4c$chi2)
ARI(part_4c_mel, part_cah_4c$gower)
ARI(part_4c_mel, part_cah_4c$manhattan)

#Partition la plus proche gower
knitr::kable(table(part_4c_mel,part_cah_4c$gower, deparse.level = 2))
```


##### Choix final de la partition

##### Retour sur la visualisation

##### Graphique de l'ACM avec classes déterminées dans la partition

# Annexe : Table des modalités

```{r}
DT::datatable(readRDS(file = paste0(chemin_data,"labels_modalites_variables.rds")),
              caption = "Tables des variables utilisées et de leur modalité",
              rownames = FALSE)
```


