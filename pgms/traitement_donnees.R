library(tidyverse)
library(magrittr)

#Importation des données du projet de stat descriptive####
#La table des établissements####
schools <- haven::read_sas("data/schools.sas7bdat")
str(schools)
#les variables disponibles dans schools
View(sapply(names(schools), FUN = function(x) attr(schools[[x]], "label")))


#Formatage des variables
schools %<>%
  mutate_at(vars(school_ident), as.character) %>%
  mutate_at(vars(school_estim_stu_with_needs,school_estim_stu_disadvanta,school_size_of_class,school_ratio_stud_teach)
            ,funs(as.numeric)) %>% 
  mutate_at(vars(starts_with("school_adm_")), 
            funs(factor), levels = 1:3, labels = c("Jamais","Parfois","Toujours"), ordered = TRUE) %>%
  mutate(school_status = factor(school_status, levels = 1:2, labels = c("Public","Private"))) %>%
  mutate_at(vars(starts_with("school_pb_")),
            funs(factor)
            , levels = 1:4
            , labels = c("Pas du tout","Très peu", "Dans une certaine mesure","Beaucoup")
            , ordered = TRUE) %>%
  mutate_at(vars(starts_with("school_pb_")),
            funs(fct_collapse)
            , `Pas ou peu de problèmes` = c("Pas du tout","Très peu")
            , `Plutôt ou beaucoup de problèmes` = c("Dans une certaine mesure","Beaucoup")) %>%
  mutate_at(vars(school_type),
            funs(factor),
            levels = c("FRA0101","FRA0202","FRA0203","FRA0204","FRA0205","FRA0206"),
            # labels = c("Lycée GT",rep("Coll., lyc. pro ou agr.",3), rep("Autres",2)),
            ordered = TRUE) %>%
  mutate_at(vars(school_type),
            funs(fct_collapse),
            Lyc = c("FRA0101"),
            Colagripro = c("FRA0202","FRA0203","FRA0204"),
            Autres = c("FRA0205","FRA0206")
            ) %>%
  mutate_at(vars(starts_with("school_size_of_city")),
            funs(factor)
            , levels = 1:5
            , labels = c("Collectivité de moins de 3 000 habitants"
                         ,"Bourg de 3 000 à 15 000 habitants"
                         ,"Petite ville de 15 000 à 100 000 habitants"
                         ,"Ville de 100 000 à 1 000 000 habitants"
                         ,"Agglomération de plus d'un million d'habitants")
            , ordered = TRUE) %>%
  mutate_at(vars(starts_with("school_size_of_city")),
            funs(fct_collapse),
            `Moins de 15 000 habitants` = c("Collectivité de moins de 3 000 habitants"
                                            ,"Bourg de 3 000 à 15 000 habitants")
  )
            

#dimension de la table schools
dim(schools)
#résumé stat de la table schools
summary(schools[,-1])
#les établissements dont au moins une donnée est manquante
schools[sapply(1:nrow(schools), FUN = function(x) any(is.na(schools[x,]))),]
#Nombre de données manquantes par variable
nb_na_by_var <- sapply(1:ncol(schools), FUN = function(x) table(is.na(schools[,x]))["TRUE"])
names(nb_na_by_var) <- names(schools)

#Traitement temporaire (à valider ou non) des données manquantes
#choix  : on supprime toute ligne comprenant une valeur manquante
#la comparaison des résumés stats semble indiquer aucune déformation importante de l'info
#on conserve le ratio pub/privé et 
sch_sans_na <- schools[sapply(1:nrow(schools), FUN = function(x) all(!is.na(schools[x,]))),]
dim(sch_sans_na)
summary(sch_sans_na)

lycGT_sans_na <- sch_sans_na %>% filter(school_type == "Lyc")
dim(lycGT_sans_na )
summary(lycGT_sans_na)
plot(lycGT_sans_na %>% select_if(is.numeric))

lyceesGT <- lycGT_sans_na %>%
  select(school_ident, school_size_of_city, school_status, starts_with("school_pb_lackof"),
         contains("_estim_"), school_size_of_class, school_ratio_stud_teach)

by(lyceesGT, lyceesGT$school_status, summary)

#La table des étudiants####

View(sapply(names(schools), FUN = function(x) attr(schools[[x]], "label")))


students <- haven::read_sas("data/students.sas7bdat") 
str(students)

etudiants <- students %>%
  mutate_at(vars(school_ident), as.character) %>%
  mutate_at(vars(student_gender),
            funs(factor), levels = 1:2, labels = c("Femme","Homme"), ordered = FALSE) %>%
  mutate_at(vars(student_level),
            funs(factor), levels = 7:12,
            labels = c("5è","4è","3è","2nde","1ère","Tle"),
            ordered = TRUE) %>%
  mutate_at(vars(contains("desk_for_work"),contains("room_self"),contains("quiet_work"), contains("dictionary"))
            ,funs(factor)
            ,levels = 1:2
            ,labels = c("oui","non")
            ,ordered = FALSE) %>%
  mutate_at(vars(contains("useful_science_"))
            ,funs(factor)
            ,levels = 1:4
            ,ordered = TRUE) %>%
  mutate_at(vars(contains("useful_science_"))
            ,funs(fct_collapse)
            ,Daccord = c("1","2")
            ,Pasdaccord = c("3","4")) %>%
  mutate(students_score_lecture = 
           (PV1READ+PV2READ+PV3READ+PV4READ+PV5READ+PV6READ+PV7READ+PV8READ+PV9READ+PV10READ)/10
  ) %>%
  mutate(student_aimed_diploma = factor(student_aimed_diploma,
                                        levels = c(1:3,5:6),
                                        ordered = TRUE
                                        )) %>%
  mutate_at(vars(student_aimed_diploma)
            ,funs(fct_collapse)
            ,MoinsBac = c("1","2")
            ,Bac = "3"
            ,BTS = "5"
            ,LicPlus = "6") %>%
  mutate(student_repetition = factor(student_repetition,
                                     levels = 0:1,
                                     labels = c("n","o"),
                                     ordered = TRUE
  )) %>%
  select(-student_age,-contains("_useful_"), -household_income, -starts_with("parents_"),
         -contains("READ"), -student_diploma_level_parents, -household_spent_for_school, -household_possession)

summary(etudiants)
summary(etudiants %>% filter(student_level %in% c("2nde","1ère","Tle")))

table(etudiants$student_weight, useNA = "always")
#les variables disponibles dans students
# View(sapply(names(init_schools), FUN = function(x) attr(init_schools[[x]], "label")))

#fusion 

lyceensGT <- etudiants %>%
  right_join(lyceesGT, by = "school_ident") %>%
  #quelques élèves sont encore notés en 3e ou 4e, on les supprime pour être sûr de ne conserver que les lycéens GT
  filter(student_level %in% c("2nde","1ère","Tle"))

#déciles de niveau social des lycéens du GT
niveaux_soc <- lyceensGT$household_index_socio_eco
summary(niveaux_soc)
plot(ecdf(niveaux_soc))
n <- length(niveaux_soc)
sd(niveaux_soc,na.rm=TRUE)*(n-1)/n

hist(niveaux_soc, probability = TRUE)
curve(dnorm(x,mean(niveaux_soc,na.rm = TRUE), sd(niveaux_soc, na.rm = TRUE)*(n-1)/n), add = TRUE, col = "blue")

points <- c(min(niveaux_soc,na.rm = TRUE),
            mean(niveaux_soc,na.rm=TRUE)-sd(niveaux_soc,na.rm=TRUE)*(n-1)/n,
            # mean(niveaux_soc,na.rm=TRUE)-1/2*sd(niveaux_soc,na.rm=TRUE)*(n-1)/n,
            mean(niveaux_soc,na.rm=TRUE), 
            # mean(niveaux_soc,na.rm=TRUE)+1/2*sd(niveaux_soc,na.rm=TRUE)*(n-1)/n, 
            mean(niveaux_soc,na.rm=TRUE)+sd(niveaux_soc,na.rm=TRUE)*(n-1)/n,
            max(niveaux_soc,na.rm=TRUE))

niveaux_soc_tr1 <- cut(niveaux_soc, breaks = points,include.lowest = TRUE, right = FALSE, ordered_result = TRUE)
table(niveaux_soc_tr1)
prop.table(table(niveaux_soc_tr1))*100

quantile(niveaux_soc, na.rm=TRUE)

lyceensGT %<>%
  mutate(niveau_social_tr = cut(household_index_socio_eco,
                                breaks = points,
                                labels = paste0("NIV_0",1:4),
                                include.lowest = TRUE, right=FALSE,ordered_result = TRUE)
  ) %>%
  select(-household_index_socio_eco)

#scores en lecture et construction des tranches de scores
summary(lyceensGT$students_score_lecture)
scores <- lyceensGT$students_score_lecture

plot(ecdf(scores))
n <- length(scores)
# sd(scores_sans_na)*(n-1)/n

hist(scores, probability = TRUE)
curve(dnorm(x,mean(scores), sd(scores)*(n-1)/n), add = TRUE, col = "blue")

qqnorm(scores)
qqline(scores)

boxplot(scores)

#La distribution des scores suit assez bien une gaussienne. Les queues de distribution étant moins bien représentées.
#on choisit deux classements :
#1 - classes avec la moyenne et l'écart type : 
#très peu de valeurs (3%) en dessous de 2 sd en-dessous de la moyenne => on regroupe
#très peu de valeurs (1%) au dessus de 2 sd au dessus de la moyenne => on regroupe
points <- c(min(scores),
            mean(scores)-sd(scores)*(n-1)/n,
            mean(scores), 
            mean(scores)+1*sd(scores)*(n-1)/n,
            max(scores))
scores_tr1 <- cut(scores,
    breaks = points,
    labels = paste0("SCG_0",1:4),
    include.lowest = TRUE,
    right = FALSE,
    dig.lab = 0,
    ordered_result = TRUE)

table(scores_tr1)    
prop.table(table(scores_tr1))*100

lyceensGT %<>%
  mutate(students_score_tr = cut(students_score_lecture,
                                breaks = points,
                                labels = paste0("NIV_0",1:4),
                                include.lowest = TRUE, right=FALSE,ordered_result = TRUE)
  )

#2 - classes avec quartiles
# scores_tr2 <- cut(scores,
#                   breaks = quantile(scores),
#                   labels = paste0("SCQ_",1:4),
#                   include.lowest = TRUE,
#                   right = FALSE,
#                   dig.lab = 0,
#                   ordered_result = TRUE)
# 
# quantile(scores)
# table(scores_tr2)    
# prop.table(table(scores_tr2))*100

#tranche de la variable student_outhours
lyceensGT %<>%
  mutate(student_outhours = cut(student_outhours, 
                                breaks = quantile(student_outhours, na.rm = TRUE),
                                labels = paste0("Q_",1:4),
                                include.lowest = TRUE, 
                                right = FALSE,
                                ordered_result = TRUE
  ))

prop.table(table(lyceensGT$student_outhours,useNA = "always"))*100
prop.table(table(lyceensGT$student_outhours,useNA = "no"))*100

#tranches des variables schools
lyceensGT %<>%
  mutate(school_estim_stu_with_needs = cut(school_estim_stu_with_needs, 
                                breaks = quantile(lyceensGT$school_estim_stu_with_needs, probs = c(0,1/2,3/4,1), na.rm = TRUE),
                                labels = paste0("Q_",2:4),
                                include.lowest = TRUE, 
                                right = FALSE,
                                ordered_result = TRUE
  )) %>%
  mutate(school_estim_stu_disadvanta = cut(school_estim_stu_disadvanta, 
                                           breaks = quantile(school_estim_stu_disadvanta, na.rm = TRUE),
                                           labels = paste0("Q_",1:4),
                                           include.lowest = TRUE, 
                                           right = FALSE,
                                           ordered_result = TRUE
  )) %>%
  mutate(school_ratio_stud_teach = cut(school_ratio_stud_teach, 
                                           breaks = c(0,median(school_ratio_stud_teach),22),
                                           labels = c("infMed","supMed"),
                                           include.lowest = TRUE, 
                                           right = FALSE,
                                           ordered_result = TRUE
  )) %>%
  select(-school_size_of_class) #les lycées ont quasiment tous 33 élèves par classe
  # mutate(school_size_of_class = cut(school_size_of_class, 
  #                                          breaks = quantile(school_size_of_class, probs = c(0,1/4,3/4,1), na.rm = TRUE),
  #                                          labels = paste0("Q_",c(1,3,4)),
  #                                          include.lowest = TRUE, 
  #                                          right = FALSE,
  #                                          ordered_result = TRUE
  # ))

#gestion de certaines données manquantes
#variable litt_at_home : que des 1 et des NA => on fait le choix d'imputer 0 aux non répondants
lyceensGT %<>% 
  mutate(student_litt_at_home = ifelse(is.na(student_litt_at_home), 0, student_litt_at_home)) %>%
  mutate(student_litt_at_home = factor(student_litt_at_home, levels = 0:1, labels = c("n","o"), ordered = TRUE))

summary(lyceensGT)
#Renommage des variables pour des noms plus courts
source("pgms/relabel.R", encoding = "utf-8")
p <- sapply(unique(data_labels_modalites_variables$varlong), func_relabel)
names(lyceensGT) <- c(names(lyceensGT)[1:2],var_noms_courts[1:9],
                      names(lyceensGT)[c(12)],var_noms_courts[10],
                      names(lyceensGT)[14],var_noms_courts[11:21])
summary(lyceensGT)


#export données
saveRDS(lyceensGT, file = "data/lyceensGT.rds")
save(lyceensGT, file = "data/lyceensGT.RData")

#export table avec autant de lignes que pondération
lyceensGT_pond <- lyceensGT %>% uncount(round(student_weight))
# dim(lyceensGT_pond)
# sum(lyceensGT$student_weight)
saveRDS(lyceensGT_pond, file = "data/lyceensGT_pond.rds")
save(lyceensGT_pond, file = "data/lyceensGT_pond.RData")


#export table avec les NA imputés aux valeurs correspondant à la distribution des répondants

#Importation des données initiales####
# init_schools <-  haven::read_sas("../data/pisa_fr2015_sch.sas7bdat")
# str(init_schools)
# #les variables disponibles
# View(sapply(names(init_schools), FUN = function(x) attr(init_schools[[x]], "label")))
# #les variables numériques