

library(tidyverse)
load("data/lyceensGT.RData")

var_nom_orig <- lyceensGT %>% select_if(is.factor) %>% names()

var_noms_courts <- gsub("_for_work|_work|_diploma|_self|size_of_|_at_home","",
     gsub("_lackof_","",gsub("student_|students_","stu", gsub("school_","sch",var_nom_orig))))

var_noms_courts[c(2,6,8,9,12,13,15:21)] <- c("genre","studict","sturep","stuout","schstat",
                                                   "schpbens","schpbmat","schpbinfra","schneeds","schdisav","schratio",
                                                   "famnivsoc","stuscor")

modlib_sch_pb <- c("Pas ou peu de problèmes", "Plutôt ou beaucoup de problèmes") 
mod_sch_pb <- c("n","o")
modlib_sch_status <- c("Public","Private")
mod_sch_status <- c("pu","pr")

modlib_size_city <- c("Moins de 15 000 habitants"
                      ,"Petite ville de 15 000 à 100 000 habitants"
                      ,"Ville de 100 000 à 1 000 000 habitants"
                      ,"Agglomération de plus d'un million d'habitants")
mod_size_city <- c("pet","moy","gd","trgd")

modlib_genre <- c("Femme","Homme")
mod_genre <- c("f","h")

modlib_dipl <-c( "MoinsBac","Bac","BTS","LicPlus")
mod_dipl <- c("infbac","bac","bts","lic+")

modlib_level <- c("5è","4è","3è","2nde","1ère","Tle")

modlib_home <- c("oui","non")
mod_home <- c("o","n")

modlib_nivsoc <- paste0("NIV_0",1:4)
mod_nivsoc <- paste0("niv",1:4)

modlib_score <- paste0("NIV_0",1:4)
mod_score <- paste0("niv",1:4)

libq <-  paste0("Q_",1:4)
q <-  paste0("q",1:4)

modlib_ratio <- c("infMed","supMed")
mod_ratio <- c("inf","sup")

data_labels_modalites_variables <- data.frame(
  var = var_noms_courts,
  varlong = var_nom_orig,
  nbmod = c(6,2,2,2,2,2,4,2,4,2,4,2,2,2,2,2,3,4,2,4,4),
  stringsAsFactors = FALSE
) %>% uncount(nbmod) %>%
  mutate(
    modlib = c(modlib_level,modlib_genre,rep(modlib_home,4),modlib_dipl,
               modlib_home,libq,modlib_home,modlib_size_city,modlib_sch_status,rep(modlib_sch_pb,4),
               libq[-1],libq,modlib_ratio,rep(modlib_nivsoc,2))
  ) %>%
  mutate(
    mod = c(modlib_level,mod_genre,rep(mod_home,4),mod_dipl,
               mod_home,q,mod_home,mod_size_city,mod_sch_status,rep(mod_sch_pb,4),
               q[-1],q,mod_ratio,rep(mod_nivsoc,2))
  )

saveRDS(data_labels_modalites_variables, file = "data/labels_modalites_variables.rds")

func_relabel <- function(variable){
  print(summary(lyceensGT[[variable]]))
  modalites <- data_labels_modalites_variables %>% 
    filter(varlong == variable) %>%
    select(contains("mod"))
  niveaux <- levels(lyceensGT[[variable]])
  levels(lyceensGT[[variable]])[which(niveaux == modalites$modlib)] <<- modalites$mod
  print(summary(lyceensGT[[variable]]))
}

# variable <- "student_gender"
# func_relabel("student_repetition")

# p <- sapply(unique(data_labels_modalites_variables$varlong), func_relabel)
# 
# names(lyceensGT) <- c(names(lyceensGT)[1:2],var_noms_courts[1:9],
#                       names(lyceensGT)[c(12)],var_noms_courts[10],
#                       names(lyceensGT)[14],var_noms_courts[11:21])
#                       
# summary(lyceensGT)
